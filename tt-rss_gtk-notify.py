#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       tt-rss_gtk-notify.py
#
#       Copyright 2011 Jonas <jonas@Portable-Jonas>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.


__title__ ="tt-rss_gtk-notify";
__author__ ="Jonas Fourquier"
__version__ ="0.20151113"
# CHANGELOG
# 0.20110802: first release
# 0.20121104: update to 1.5.3 API (json way)
# 0.20121107: pycurl remplaced by urllib2
# 0.20150208: Undisplay resums of article (really more reactive with lots of articles)
# 0.20151113: simplejson remplaced by json
__homepage__ = ""
__usage__ ='''
%(title)s
Version: %(version)s Author: %(author)s

Usage: %(file)s [option]

Options:
  -h, --help            show this help message and exit
'''%{'title':__file__, 'file':__file__, 'version':__version__,'author':__author__}

import pygtk, gtk, gobject, cairo, sys, webbrowser,locale, os, gettext, json
from datetime import datetime, date, timedelta
from ConfigParser import ConfigParser
import pynotify
import logging
import urllib2

#debuger
logging.basicConfig(level=logging.ERROR,format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__title__)

#locales
lang = locale.getdefaultlocale()
locale.setlocale(locale.LC_ALL,lang)

#gettext
cur_lang = gettext.translation(__title__, localedir=os.path.join(os.path.dirname(__file__),'locale'), languages=[lang[0][:2]])
cur_lang.install()
N_ = gettext.ngettext

session_id = None
lastUpdate = 0
lastNumUnread = 0
lastNumNews= 0

def check_unread(forceDisplay=False) :
    global session_id, lastUpdate, lastNumUnread, lastNumNews

    def request(data_dict) :
        req = urllib2.Request(server+'/api/',json.dumps(data_dict))
        rep = urllib2.urlopen(req)
        return json.loads(rep.read())

    #login
    logging.debug("login ...")
    if not session_id :
        data = request({'op': 'login', 'user': username, 'password': password})
        if data['status'] == 0 :
            session_id = data['content']['session_id']
        else :
            gobject.timeout_add(refresh, check_unread,True)
            return

    #check numUnread
    logging.debug("check numUnread ...")
    data = request({'op': 'getUnread', 'sid': session_id})
    if data['status'] == 0 :
        numUnread = data['content']['unread']
    else :
        numUnread = 0

    # check last articles
    logging.debug("check last articles ...")
    data = request({'op': 'getHeadlines', 'feed_id': -3, 'sid': session_id})
    if data['status'] == 0 :
        thisUpdate = lastUpdate
        numNews = len(data['content'])
        content = []

        today = date.today()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)
        previousDatetimeArticle = today
        todayHasPrinted = False
        yesterdayHasPrinted = False
        tomorrowHasPrinted = False
        for article in data['content'] :
            thisUpdate = max(article['updated'],thisUpdate)
            datetimeArticle = datetime.fromtimestamp(article['updated'])
            # add the day in content
            if datetimeArticle.day == today.day and datetimeArticle.month == today.month and datetimeArticle.year == today.year :
                if not todayHasPrinted :
                    content.append(markupDate%_('Aujourd\'hui'))
                    todayHasPrinted = True
            elif datetimeArticle.day == yesterday.day and datetimeArticle.month == yesterday.month and datetimeArticle.year == yesterday.year :
                if not yesterdayHasPrinted :
                    content.append(markupDate%_('Hier'))
                    yesterdayHasPrinted = True
            elif datetimeArticle.day == tomorrow.day and datetimeArticle.month == tomorrow.month and datetimeArticle.year == tomorrow.year :
                if not tomorrowHasPrinted :
                    content.append(markupDate%_('Demain'))
                    tomorrowHasPrinted = True
            elif datetimeArticle.day != previousDatetimeArticle.day or datetimeArticle.month != previousDatetimeArticle.month or datetimeArticle.year == previousDatetimeArticle.year :
                content.append(markupDate%datetimeArticle.strftime(fDate))
                previousDatetimeArticle = datetimeArticle
            # add article in content
            content.append(markupTime%datetimeArticle.strftime(fTime)+markupTitle%article['title'])

    # update notify
    logging.debug("update notify ...")
    notifyHasChanged = False
    if numNews != lastNumNews or numUnread != lastNumUnread or forceDisplay :
        if numUnread != lastNumUnread or forceDisplay :
            icon = draw_icon(numNews)
            n.set_icon_from_pixbuf(icon)
        label = N_('%s nouveau','%s nouveaux',numNews)%numNews+'\n'+N_('%s non lu','%s non lus',numUnread)%numUnread
        #~ n.update(title + ' ' + label, "\n".join(content))
        n.update(title + ' ' + label)
        lastNumNews = numNews
        lastNumUnread = numUnread
        notifyHasChanged = True

    # display notify
    if thisUpdate > lastUpdate or forceDisplay :
        logger.info('they are news articles, display notification')
        lastUpdate = thisUpdate
        n.show()
    elif notifyHasChanged :
        logger.info('they are not news articles but num unread or num new has changed, update notification')
        # FIXME maj notification sans l'afficher
        n.show()
    else :
        logger.info('they are not news articles, don\'t display notification')

    # next check in <refresh> microsecond
    gobject.timeout_add(refresh, check_unread)

def on_openBt_pressed (n,action) :
    webbrowser.open(server)

def draw_icon(nbitem=0) :
    surface = cairo.ImageSurface.create_from_png(os.path.join(os.path.dirname(__file__),'icon.png'))
    if nbitem :
        nbitem = str(nbitem)
        cr = cairo.Context(surface)
        cr.set_line_width(40)
        cr.set_font_size(26)
        x, y, w, h = cr.text_extents(nbitem)[:4]
        cr.set_source_rgba(0,0,0,.8)
        cr.rectangle(60-w,60-h,64,64)
        cr.fill()
        cr.set_source_rgb(1,1,1)
        cr.move_to(62-w,62)
        cr.text_path(nbitem)
        cr.fill()
    surface.write_to_png(tmpdir+'/icon_tt-rss_gtk-notify.png')
    pixbuf = gtk.gdk.pixbuf_new_from_file(tmpdir+'/icon_tt-rss_gtk-notify.png')
    return pixbuf

def displayError(msg) :
    logging.error(msg)
    nerror = pynotify.Notification(title + " - " + _('Erreur'),"<u>"+datetime.now().strftime('%x %X')+"</u>\n"+str(msg))
    nerror.set_icon_from_pixbuf(draw_icon('E'))
    nerror.show()

if __name__ == '__main__':
    #read config
    try :
        config = ConfigParser()
        config.read([
                os.path.join(os.path.expanduser('~'),'.tt-rss_gtk-notify.cfg'),
                os.path.join(os.path.expanduser('~'),'.config','tt-rss_gtk-notify.cfg'),
                os.path.join(os.path.dirname(__file__),'tt-rss_gtk-notify.cfg')
            ])
        title = config.get('apparence','title')
        server = config.get('connection','server')
        username = config.get('connection','username')
        password = config.get('connection','password')
        refresh = int(config.getfloat('connection','refresh')*60000)
        markupTitle = config.get('apparence','markupTitle')
        markupDate = config.get('apparence','markupDate')
        markupTime = config.get('apparence','markupTime')
        fDate = config.get('apparence','fDate')
        fTime = config.get('apparence','fTime')
        tmpdir = '/tmp'
    except Exception, e :
        displayError(e)

    pynotify.init (title)
    #notication
    n = pynotify.Notification(title,_('loading'))
    n.add_action('open',_('Open'),on_openBt_pressed)
    n.set_hint("resident",True)
    n.set_hint("persistence",True)

    loop = gobject.MainLoop ()
    check_unread(forceDisplay=True)
    loop.run ()
